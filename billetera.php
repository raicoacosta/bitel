<!DOCTYPE html>
<html>
<?php include "partials/head.php"; ?>
<body>	
	<?php include "partials/header.php"; ?>
	
	<!--BODY-->

		<div class="img-banner">
	    	<div class="col-md-12 no-padding">		
	    		<img class="img-responsive" src="images/banner-nosotros.png" alt="La agencia">	
		    </div>
		    <ol class="breadcrumb">
	  			<li><a href="#">Home</a></li>
		  		<li class="active">Tu Billetera</li>
			</ol>
			<h2 class="title-banner"><strong>TU BILLETERA</strong></h2>
    	</div>

		<div class="section-main-block container pb-0">
			<div class="col-md-12">		
	     		<p class="subtitle-about text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</p>
	     	</div>
	     	<div class="col-md-4 col-md-offset-1 hidden-xs hidden-sm">
	 			<img class="" src="images/phone-banner.png" alt="Celular">	
			</div>
	     	
	     	<div class="text-about col-md-6 col-md-offset-1">
				<h4 class="text-color"><strong>AFILIATE</strong></h4>
				<div class="col-md-10">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
				
			</div>	        
	     	<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
	 			<img class="img-responsive img-padding" src="images/phone-banner.png" alt="Celular">	
			</div>
		</div>
		<div class="section-main-block container pb-0 gray">
			<div class="benefit gray">
				<div class="col-xs-10 col-xs-offset-1">
					<h1 class="title-section"><strong>BENEFICIOS DE LA BILLETERA</strong></h1>
				</div>
				<div class="hidden-xs hidden-sm col-md-2 col-md-offset-2 colum-benefit">
					<div class="row">
						<img src="images/number-one.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row colum-benefit-space">
						<img src="images/number-two.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row">
						<img src="images/number-three.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 beneficio-img hidden-xs hidden-sm">
					<img class="img-responsive center-block img-beneficio" src="images/chico-beneficio.png">
				</div>
				<div class="hidden-xs hidden-sm col-md-2 col-md-offset-0 colum-benefit">
					<div class="row colum-benefit-space-alter">
						<img src="images/number-four.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row">
						<img src="images/number-five.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row colum-benefit-space-alter">
						<img src="images/number-six.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
				</div>
			</div>
			<div class="hidden-md hidden-lg">
			    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			        <!-- Wrapper for slides -->
			        <div class="carousel-inner">
			            <div class="item active">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail adjust-item-gray">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-one.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail adjust-item-gray">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-two.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail adjust-item-gray">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-three.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail adjust-item-gray">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-four.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail adjust-item-gray">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-five.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail adjust-item-gray">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-six.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <!-- Controls -->
			        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
			        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
			    </div>
			</div>

			<div class="beneficio-img col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 hidden-md hidden-lg">
				<img class="img-responsive center-block" src="images/chico-beneficio.png">
			</div>	
		</div>
		<?php include "partials/download-app.php"; ?>

	<!--/BODY-->

	<?php include "partials/footer.php"; include "partials/scripts.php"; ?>
</body>
</html>
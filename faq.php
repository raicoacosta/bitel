<!DOCTYPE html>
<html>
<?php include "partials/head.php"; ?>
<body>	
	<?php include "partials/header.php"; ?>
	
	<!--BODY-->
	    <div class="img-banner">
		    <div class="col-md-12 no-padding">		
		    	<img class="img-responsive" src="images/banner-globos.png" alt="La agencia">	
		    </div>
		    <ol class="breadcrumb">
	  			<li><a href="#">Home</a></li>
		  		<li class="active">FAQ</li>
			</ol>
			<h2 class="title-banner"><strong>FAQ</strong></h2>
	    </div>
	    <section class="container">
	    	<div class="faq">
	    		<div class="row">
	    			<div class="col-md-10 col-md-offset-1 ">
	    				<h1 ><strong>PREGUNTAS FRECUENTES</strong></h1>
	    				<div class="panel-group" id="acordeon">
	    					<div class="panel panel-default">
	    						<div class="panel-heading panel-subtitulos" id="heading1">
	    							<h4 class="panel-title">
	    								<a class="arrow" href="#colapsable1" data-toggle="collapse" data-parent="#acordeon">INSCRIPCIÓN<span class="caret caret-open"></span></a>
	    							</h4>
	    						</div>

	    						<div id="colapsable1" class="panel-collapse collapse in" arial-labelledby="heading1">		
	    							<div class="panel-body">
										<h3><strong>¿Cuáles son los requisitos para inscribirme a la billetera bitel?</strong></h3>
										<p>Los requisitos son:
										1. Ser persona natural de nacionalidad peruana, mayor de 18 años.
										2. Poseer un celular (puede ser uno simple), que se encuentre activo con las operadoras: Movistar, Claro, Entel o Bitel. Si por incumplimiento de pago u otros motivos la Operadora desactiva o bloquea la línea, no se podrá usar la billetera electrónica.
										3. Tener cobertura de señal celular, lo cual NO es responsabilidad de la Billetera Electrónica.
										4. Realizar operaciones sólo en Moneda Nacional (SOL).
										</p>
										<h3><strong>¿Puedo inscribirme con más de un celular a la billetera bitel?</strong></h3>
										<p>No, solo puedes tener una billetera electrónica bitel asociada a un mismo número de celular y número de DNI.</p>

										<h3><strong>¿Cómo me inscribo?</strong></h3>
										<p>Es muy simple, desde tu celular puedes llamar al número corto *147# o descargar la Aplicación billetera bitel totalmente gratuita en Google Play o App Store; y seguir estos pasos:

										1. Ingresa tu número de DNI y tu año de nacimiento, conforme te lo vamos solicitando.
										2. Verifica que la información mostrada es correcta.
										3. Crea una Clave Secreta, la cual es un número de 4 dígitos no consecutivos (1234) ni iguales (1111). 
										Al ingresar la Clave Secreta estás aceptando los términos y condiciones del uso del servicio, los cuales están especificados en el "Contrato", el mismo que puedes descargar desde está página web. 
										4. Vuelve a ingresar la Clave Secreta; y listo, ya tienes tu billetera bitel. 

										Si necesitas más ayuda puedes comunicarte a la línea telefónica del Centro de Atención al Usuario: (01) 702-8070, o mandar un correo electrónico a atencionalusuario@gmoney.com.pe
										</p>
										<h3><strong>¿Es necesario un monto mínimo de dinero para abrir mi billetera bitel?</strong></h3>
										<p>No, la inscripción a la billetera bitel es totalmente gratuita</p>

										<h3><strong>¿Qué es el código de validación?</strong></h3>
										<p>Es un código de cuatro números que recibirás por mensaje de texto (SMS), y que sirve para validar tu número de celular.</p>

										<h3><strong>¿Qué es y para qué sirve la clave secreta?</strong></h3>
										<p>La Clave Secreta es un mecanismo de seguridad, con la cual sólo tú autorizas la realización de operaciones con tu celular.
										Esta Clave consta de 4 dígitos, no consecutivos (1234) ni repetidos (1111). Además, debe ser personal e intransferible, siendo tú el único responsable de su cuidado y privacidad.
										Te recomendamos que cambies tu Clave Secreta periódicamente o cuando sospeches de su vulnerabilidad.
										</p>
										 <h3><strong>¿Qué hago si me olvido la Clave Secreta?</strong></h3>
										<p>Con la App Billetera Bitel.
										1. Presionar en la opción: ¿Olvidaste tu contraseña?
										2. Ingresar la respuesta a la pregunta (esta pregunta la seleccionaste y respondiste al momento de tu inscripción.
										3. Crea una nueva Clave Secreta.

										Con el número corto: *147#
										Deberás comunicarte a la línea telefónica del Centro de Atención al Usuario: teléfono (01) 702-8070 o mandar un correo electrónico a atencionalusuario@gmoney.com.pe y solicitar el "Restablecimiento de Clave Secreta".
										El Auxiliar de Atención autenticará tu condición de usuario de la billetera bitel y generará una Clave Secreta Provisional, la cual se envía automáticamente por SMS a tu celular.
										Cabe resaltar que, por tu seguridad, la Clave Secreta Provisional expirará luego de 5 min de ser enviada; por ello es necesario que en cuanto la recibas marques el *147# en tu celular y sigas los pasos para cambiar esa clave por una Clave Secreta Personal.
										</p>

										<h3><strong>¿Para qué sirve la pregunta para recuperar mi Clave Secreta?</strong></h3>
										<p>La respuesta a la pregunta selecciona al momento de la inscripción, es un mecanismo de seguridad no presencial que sirve para autenticar tu condición de usuario de la billetera bitel.
										Si no recuerdas la respuesta comunícate al Centro de Atención al Usuario, llamando al (01) 702-8070.</p>
	    							</div>
	    						</div>
	    					</div>

	    					<div class="panel panel-default">
	    						<div class="panel-heading panel-subtitulos" id="heading2">
	    							<h4 class="panel-title">
	    								<a class="arrow" href="#colapsable2" data-toggle="collapse" data-parent="#acordeon">TRÁMITES Y CONSULTAS<span class="caret"></span></a>
	    							</h4>
	    						</div>

	    						<div id="colapsable2" class="panel-collapse collapse" arial-labelledby="heading2">
	    							<div class="panel-body">
	    								<h3><strong>¿Si cambio de número de celular, mantengo el servicio de mi billetera bitel?</h3></strong>
										No, tu billetera bitel está asociada a tu número de celular y tu número de DNI.
										Si cambias de número de celular, tendrás que seguir estos pasos:
										1. Sacar todo el Saldo de tu billetera bitel. Esto lo puedes hacer con:
										Hacer un Giro. Para enviar tu plata a otro número de celular.
										Pagar un servicio.
										Recargar un celular.
										Hacer una compra.
										Sacar plata. Para retirar tu plata en un agente.
										2. Cancelar tu billetera bitel. Para ello deberás comunicarte a la línea telefónica del Centro de Atención al Usuario: (01) 702-8070, o mandar un correo electrónico a: atencionalusuario@gmoney.com.pe, donde deberás indicar que la cancelación de tu billetera bitel se debe al cambio de tu número celular.
										3. Volverte a inscribir a tu billetera bitel con el nuevo número celular.

										<h3>¿Si cambio de operador móvil, pero no de número de celular, mantengo el servicio de mi billetera bitel?</h3><strong></strong>
										No, la billetera bitel es exclusiva para clientes Bitel. 

										<h3><strong>¿Qué pasa si me roban o pierdo mi celular?</h3></strong>
										No te preocupes, recuerda que tu plata está segura, ya que nadie podrá usar tu billetera bitel sin tu Clave Secreta. 
										En caso de pérdida o robo comunícate con la línea telefónica del Centro de Atención al Usuario: (01) 702-8070 y solicitar el Bloqueo de tu billetera bitel.
										Una vez que reactives tu número de celular, solicita el Desbloqueo de tu billetera bitel.

										<h3><strong>¿Existe compromiso de permanencia en el servicio de la billetera bitel?</h3></strong>
										No, puedes cancelar tu billetera bitel en cualquier momento.

										<h3><strong>¿Se puede operar con el servicio desde el extranjero?</h3></strong>
										No, la billetera bitel es un servicio de ámbito nacional.

										<h3><strong>¿Qué hacer en caso de fallecimiento del titular?</h3></strong>
										En caso de fallecimiento, los deudos podrán reclamar el saldo de la billetera bitel, presentando la siguiente documentación:
										1. Solicitud de entrega de saldos / consulta de saldos con firma legalizada.
										2. Copia de los documentos de identidad del titular y heredero(s).
										3. Acreditar calidad de sucesores; mediante, declaratoria de herederos (sucesión intestada) o testamento (sucesión testada). Cualquiera de ellos, debidamente inscrito en el Registro Público de Sucesiones de la Superintendencia Nacional de los Registros Públicos (“SUNARP”).
										Esta información deberá ser entregada en nuestra oficina ubicada en Av. Camino Real N° 390, C.C. Camino Real, Torre Central Of. 901-B, San Isidro.
										Luego que los deudos hayan reclamado el saldo de la billetera bitel, está procederá a ser Cancelada.

										En caso, los deudos no conozcan las entidades financieras en las que el causante hubiere tenido ahorros o valores podrán solicitar una "Constancia de Depósito" a la SBS, conforme lo estipulada la Ley sobre los herederos informados en los Servicios Financieros Pasivos - Ley No. 30152. Para mayor información visite la página web de la SBS (www.sbs.gob.pe / Portal del Usuario / Atención al Usuario / Plataforma / Constancia de Depósitos.) o alguna oficina de la SBS.

										<h3><strong>¿Cómo puedo cancelar mi billetera bitel?</h3></strong>
										Para Cancelar tu billetera bitel deberás comunicarte a la línea telefónica del Centro de Atención al Usuario: teléfono (01) 702-8070 o enviar un correo electrónico a: atencionalusuario@gmoney.com.pe
										Para realizar esta operación asegúrate de no tener saldo en tu billetera bitel.

										Cabe mencionar que una vez cancelada tu billetera, no podremos activarla nuevamente; y, si quieres volver a contar con el servicio de la billetera bitel deberá inscribirte nuevamente,

										<h3><strong>¿Cómo puedo bloquear mi billetera bitel?</h3></strong>
										Para solicitar el Bloqueo de tu billetera bitel deberás comunicarte a la línea telefónica del Centro de Atención al Usuario: (01) 702-8070, seleccionar la opción de bloqueo de billetera e ingresar los datos solicitados.
										También puedes enviar un correo a: atencionalusuario@gmoney.com.pe indicando: tu número de celular, tu número de DNI y los motivos del bloqueo.


										<h3><strong>¿Cómo puedo desbloquear mi billetera bitel?</h3></strong>
										Para solicitar el Desbloqueo de tu billetera bitel deberás comunicarte a la línea telefónica del Centro de Atención al Usuario: (01) 702-8070 y seguir los pasos que los Auxiliares de Atención te indicarán.
										También puedes enviar un correo a: atencionalusuario@gmoney.com.pe indicando: tu número de celular, tu número de DNI y los motivos del desbloqueo.

										<h3><strong>¿Cuánto cuesta usar mi billetera bitel?</h3></strong>
										Las tarifas de cada uno de los productos los puedes revisar en el Contrato o en esta página web, en la sección tarifario.

										<h3><strong>¿Cómo cambio mi Clave Secreta?</h3></strong>
										Desde tu billetera electrónica ingresa a la opción: Cambio de Clave Secreta e ingresa los datos solicitados. Si no recuerdas tu Clave Secreta actual comunícate a la línea telefónica del Centro de Atención al Usuario: (01) 702-8070
	    							</div>
	    						</div>
	    					</div>

	    					<div class="panel panel-default">
	    						<div class="panel-heading panel-subtitulos" id="heading3">
	    							<h4 class="panel-title">
	    								<a class="arrow" href="#colapsable3" data-toggle="collapse" data-parent="#acordeon">SOBRE SU USO<span class="caret"></span></a>
	    							</h4>
	    						</div>

	    						<div id="colapsable3" class="panel-collapse collapse" arial-labelledby="heading3">
	    							<div class="panel-body">
	    								<h3><strong>¿Lorem ipsum dolor sit amet?</strong></h3>
	    								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	    								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	    								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	    								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	    								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	    								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	    							</div>
	    						</div>
	    					</div>

	    					<div class="panel panel-default">
	    						<div class="panel-heading panel-subtitulos" id="heading4">
	    							<h4 class="panel-title">
	    								<a class="arrow" href="#colapsable4" data-toggle="collapse" data-parent="#acordeon">PAGA TUS SERVICIOS<span class="caret"></span></a>
	    							</h4>
	    						</div>

	    						<div id="colapsable4" class="panel-collapse collapse" arial-labelledby="heading4">
	    							<div class="panel-body">
	    								<h3><strong>¿Lorem ipsum dolor sit amet?</strong></h3>
	    								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	    								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	    								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	    								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	    								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	    								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	    							</div>
	    						</div>
	    					</div>

	    					<div class="panel panel-default">
	    						<div class="panel-heading panel-subtitulos" id="heading5">
	    							<h4 class="panel-title">
	    								<a class="arrow" href="#colapsable5 " data-toggle="collapse" data-parent="#acordeon">RECARGA TU CELULAR<span class="caret"></span></a>
	    							</h4>
	    						</div>

	    						<div id="colapsable5" class="panel-collapse collapse" arial-labelledby="heading5">
	    							<div class="panel-body">
	    								<h3><strong>¿Lorem ipsum dolor sit amet?</strong></h3>
	    								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	    								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	    								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	    								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	    								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	    								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	    							</div>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </section>
		<?php include "partials/download-app.php"; ?>
	<!--/BODY-->

	<?php include "partials/footer.php"; include "partials/scripts.php"; ?>
	<script type="text/javascript">
		$('.arrow').on('click', function(){
			$.each($('a.arrow'), function( index, value ){
				$(value).find('span').removeClass('caret-open');
			});
				if ($(this).hasClass('collapsed')){
					$(this).find('span').addClass('caret-open');
				} else{
					$(this).find('span').removeClass('caret-open');
				}
		});
	</script>
</body>
</html>
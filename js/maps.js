var locations = [
  {lat: -11.905991377952562, lng: -77.04494238852539},
  {lat: -11.875419393314722, lng: -77.12339163779296},
  {lat: -12.051245138240900, lng: -76.9932723142342},
  {lat: -13.051245138247403, lng: -76.9932737640625},
  {lat: -12.051245138247403, lng: -76.9932723140625},
  {lat: -12.051245138247403, lng: -76.9932723140625},
  {lat: -12.051245130001212, lng: -76.9932723140090},


]
function initMap() {
  var uluru = {lat: -25.363, lng: 131.044};
  var locations= this.locations;
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: locations[0]

  });

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">lorem ipsum</h1>'+
      '<div id="bodyContent">'+
      '<p> lorem ipsum</p>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: locations[0],
    map: map,
    title: 'lorem ipsum'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  var marker2 = new google.maps.Marker({
    position: locations[1],
    map: map,
    title: 'lorem ipsum'
  });

  marker2.addListener('click', function() {
    infowindow.open(map, marker2);
  });

  var marker3 = new google.maps.Marker({
    position: locations[2],
    map: map,
    title: 'lorem ipsum'
  });
  marker3.addListener('click', function() {
    infowindow.open(map, marker3);
  });
  
  var marker4 = new google.maps.Marker({
    position: locations[3],
    map: map,
    title: 'lorem ipsum'
  });
  marker4.addListener('click', function() {
    infowindow.open(map, marker4)
  })

  var marker5 = new google.maps.Marker({
    position: locations[4],
    map: map,
    title: 'lorem ipsum'
  });
  marker5.addListener('click', function() {
    infowindow.open(map, marker5);
  });

  
var marker6 = new google.maps.Marker({
  position: locations[5],
  map: map,
  title: 'lorem ipsum'
});
marker6.addListener('click', function() {
  infowindow.open(map, marker6);
});

$('.marker-list').on('click', function(){
  var mapDiv = $(this);
  var marker_list = new google.maps.Marker({
    position: locations[$(this).data('event')],
    map: map,
    title: 'lorem ipsum'
  });
  infowindow.open(map, marker_list);
});

$('#show-list').on('click', function(){
  $('a.hidden').removeClass('hidden');
});

}
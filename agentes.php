<!DOCTYPE html>
<html>
<?php include "partials/head.php"; ?>
<body>	
	<?php include "partials/header.php"; ?>
	
	<!--BODY-->
	    <section class="img-banner container-fluid no-padding">
		    <div class="col-md-12 no-padding">		
		    	<img class="img-responsive" src="images/foto-agentes.png" alt="La agencia">	
		    </div>
		    <ol class="breadcrumb">
	  			<li><a href="#">Home</a></li>
		  		<li class="active">Agentes</li>
			</ol>
			<h2 class="title-banner"><strong>AGENTES</strong></h2>
	    </section>
        <!-- Location section -->
	    <div class="location-section">
			<div class="location-map" id="location-map">
				<h1 class="text-center">Agentes Afiliados</h1>
				<div class="section-map">
					<p>Operacion con tu <strong>celular</strong></p>
					<div class="agents">
						<i class="fa fa-map-marker fa-2x "></i>
						<input type="checkbox" name=""> <label>Agentes TDM</label> 
					</div>
					<div class="deposits">
						<i class="fa fa-map-marker fa-2x "></i>
						<input type="checkbox" name=""> <label>Comercios</label> 
					</div>
				</div>
				<div class="select-map">
					<select>
						<option>Departamentos</option>
					</select>
				</div>
				<div class="select-map">
					<select>
						<option>Provincias</option>
					</select>
				</div>
				<div class="select-map">
					<select>
						<option>Distrito</option>
					</select>
				</div>
				<div class="list-location">
					<ul>
						<li> 
							<a class="marker-list hidden" data-event="1">
								<i class="fa fa-map-marker"></i> lorem ipsum
							</a> 
						</li>
						<li> 
							<a class="marker-list hidden" data-event="2">
								<i class="fa fa-map-marker"></i> lorem ipsum
							</a> 
						</li>
						<li> 
							<a class="marker-list hidden" data-event="3">
								<i class="fa fa-map-marker"></i> lorem ipsum
							</a> 
						</li>
						<li> 
							<a class="marker-list hidden" data-event="4">
								<i class="fa fa-map-marker"></i> lorem ipsum
							</a> 
						</li>
						<li> 
							<a class="marker-list hidden" data-event="5">
								<i class="fa fa-map-marker"></i> lorem ipsum
							</a> 
						</li>
						<li> 
							<a class="marker-list hidden" data-event="6">
								<i class="fa fa-map-marker"></i> lorem ipsum
							</a> 
						</li>
					</ul>
				</div>
				<div> <a id="show-list" class="btn btn-search-map text-center"> Buscar </a> 
				</div>
			</div>
            <div> <button class="toggle-visibility" id="toggle-visibility"> toggle visibility </button></div>

            <!-- End of location section -->
	    	<div id="map"></div>
	    </div>
		<section class="container section-main-block">
			<div class="locate">
					<div class="col-md-12">
						<h1><strong>AGENTES</strong></h1>
					</div>

					<div class="col-xs-12 col-md-12 agents">
						<div class="col-xs-12 col-md-6">
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<hr>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
						</div>
						<div class="col-xs-12 col-md-6">
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
							<div class="map-ubication col-md-1 col-md-offset-2 col-xs-2"><i class="fa fa-map-marker fa-2x pull-right"></i></div>
							<div class="map-ubication col-md-9 col-xs-10">
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
								<p>Lorem ipsum dolor sit amet,</p>
							</div>
						</div>
					</div>			
			</div>
		</section>
		<?php include "partials/download-app.php"; ?>
	<?php include "partials/footer.php"; include "partials/scripts.php"; ?>
    <script src="js/maps.js"></script>
    <script 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpxx7n0iNkq50vGLEf3p8FRnpvtC08iO0&callback=initMap"
    async defer></script>

    <script type="text/javascript">
    	$('#toggle-visibility').on('click', function(){
    		$('#location-map').toggleClass('location-map--no-visible');
    		$('#toggle-visibility').toggleClass('toggle-visibility--activated');

    	});
    </script>
</body>
</html>
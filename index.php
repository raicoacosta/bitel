<!DOCTYPE html>
<html>
<?php include "partials/head.php"; ?>
<body>	
	<?php 
		include "partials/header.php"; 
	?>
	
	<!--BODY-->
	    <section class="img-banner container-fluid no-padding">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
		        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
		          <img class="first-slide img-responsive" src="images/Bannerglobos.png" alt="First slide">
		        </div>
		        <div class="item left">
		          <img class="second-slide img-responsive" src="images/banner-slider1.jpeg" alt="Second slide">
		        </div>
		        <div class="item left">
		          <img class="third-slide img-responsive" src="images/banner-slider2.jpeg" alt="Third slide">
		        </div>
		      </div>
		      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		        <span class="sr-only">Previous</span>
		      </a>
		      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		        <span class="sr-only">Next</span>
		      </a>
		    </div>
	    </section>
		<section class="section-main-block gray">
			<div class="elements container no-padding">
				<h1><strong>Operaciones</strong></h1>
				<div class="col-md-8 col-md-offset-2">
					<p class="text-center subtexto">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					</p>
				</div>
				<div class="elements-content">
					<div class="col-md-2 col-md-offset-2">
						<img class="img-responsive center-block" src="images/1.png">
						<p class="text-center">Inscribete Gratis</p>
					</div>
					<div class="col-md-2">
						<img class="img-responsive center-block" src="images/2.png">
						<p class="text-center">Hacer Pagos</p>
					</div>
					<div class="col-md-2">
						<img class="img-responsive center-block" src="images/3.png">
						<p class="text-center">Haz un Giro</p>
					</div>
					<div class="col-md-2">
						<img class="img-responsive center-block" src="images/4.png">
						<p class="text-center">Meter Plata</p>
					</div>
				</div>
				<div class="elements-content">
					<div class="col-md-2 col-md-offset-2">
						<img class="img-responsive center-block" src="images/5.png">
						<p class="text-center">Paga tus Servicios</p>
					</div>
					<div class="col-md-2">
						<img class="img-responsive center-block" src="images/6.png">
						<p class="text-center">Sacar Plata</p>
					</div>
					<div class="col-md-2">
						<img class="img-responsive center-block" src="images/7.png">
						<p class="text-center">Consulta de Saldo</p>
					</div>
					<div class="col-md-2">
						<img class="img-responsive center-block" src="images/8.png">
						<p class="text-center">Consulta de Movimientos</p>
					</div>
				</div>
			</div>
		</section>
		<div class="section-main-block container pb-0">
			<div class="benefi">
				<div class="col-xs-10 col-xs-offset-1">
					<h1 class="title-section"><strong>BENEFICIOS DE LA BILLETERA</strong></h1>
				</div>
				<div class="hidden-xs hidden-sm col-md-2 col-md-offset-2 colum-benefit">
					<div class="row">
						<img src="images/number-one.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row colum-benefit-space">
						<img src="images/number-two.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row">
						<img src="images/number-three.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 beneficio-img hidden-xs hidden-sm">
					<img class="img-responsive center-block img-beneficio" src="images/chico-beneficio.png">
				</div>
				<div class="hidden-xs hidden-sm col-md-2 col-md-offset-0 colum-benefit">
					<div class="row colum-benefit-space-alter">
						<img src="images/number-four.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row">
						<img src="images/number-five.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
					<div class="row colum-benefit-space-alter">
						<img src="images/number-six.png">
						<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
					</div>
				</div>
			</div>
			<div class="hidden-md hidden-lg">
			    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			        <!-- Wrapper for slides -->
			        <div class="carousel-inner">
			            <div class="item active">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail item">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-one.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail item">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-two.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail item">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-three.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail item">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-four.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail item">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-five.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="item">
			                <div class="row">
			                    <div class="col-xs-12">
			                        <div class="thumbnail item">
										<div class="col-xs-8 col-xs-offset-2 pre-img">
											<img src="images/number-six.png">
											<p>Tu tarjeta de visa Débito CLasica cuenta con un chip inteligente que hará de tus compras más seguras</p>
										</div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <!-- Controls -->
			        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
			        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
			    </div>
			</div>

			<div class="beneficio-img col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 hidden-md hidden-lg">
				<img class="img-responsive center-block" src="images/chico-beneficio.png">
			</div>	
		</div>
		<?php 
			include "partials/download-app.php"; 
		?>
		<section class="section-main-block padding-0">
			<div class="row div-position-locate">
				<div class="col-md-6 no-padding">
		    		<div id="map"></div>
				</div>
				<div class="col-md-6 position-locate center-about-section">
		    		<h1 class="text-center">
		    			<i class="fa fa-map-marker fa-title fa-2x"></i>
		    			<span><strong>Ubícanos</strong></span>
		    		</h1>
		    		<p class="text-center">Av Javier Prado Oeste 757 Magdalena del Mar</p>
		    		<div class="text-center">
		    			<a href="agentes.php" class="btn btn-about">Agentes</a>
					</div>
				</div>
			</div>
		</section>
		<section class="section-main-block gray">
			<div class="container soport">
			    <div class='col-md-8 col-md-offset-2'>
				<h1 ><strong>ATENCIÓN AL CLIENTE</strong></h1>
     			<p class="subtitle-function text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</p>
			      <div class="carousel slide media-carousel" id="media">
			        <div class="carousel-inner">
			        	<!-- carrusel all -->
			          <div class="hidden-xs hidden-sm item  active">
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/1.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>          
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/2.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/3.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>       
			          </div>
			          <div class="hidden-xs hidden-sm item">
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/1.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>          
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/2.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/3.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>        
			          </div>
			          <div class="hidden-xs hidden-sm item">
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/1.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>          
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/2.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>
			              <div class="col-md-4">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/3.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>        
			          </div>
			          	<!-- carrusel phone -->

			          <div class="hidden-md hidden-lg item active">
			              <div class="col-xs-12">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/1.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>     
			          </div>
			          <div class="hidden-md hidden-lg item">
			              <div class="col-xs-12">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/2.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>     
			          </div>
			          <div class="hidden-md hidden-lg item">
			              <div class="col-xs-12">
			                <a class="thumbnail center-block" href="#">
			                	<img alt="" src="images/3.png"><p class="text-center">Lorem ipsum dolor sit amet </p>
			                </a>
			              </div>     
			          </div>
			        </div>
			        <a data-slide="prev" href="#media" class="left carousel-control">
			        	<span class="glyphicon glyphicon-chevron-left"></span>
			        </a>
			        <a data-slide="next" href="#media" class="right carousel-control">
			        	<span class="glyphicon glyphicon-chevron-right"></span>
			        </a>
			      </div>                          
			    </div>
			</div>
		</section>
	<!--/BODY-->
	<?php 
		include "partials/footer.php"; include "partials/scripts.php"; 
	?>

	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
		if ($(window).width() < 960) {
			$('#media .hidden-xs').removeClass('item').addClass('hide');
		} else {
			$('#media .hidden-md').removeClass('item').addClass('hide');
		}
    </script>
    <script 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpxx7n0iNkq50vGLEf3p8FRnpvtC08iO0&callback=initMap"
    async defer></script>
</body>
</html>
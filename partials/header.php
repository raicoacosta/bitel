<header>
	<div class="container-fluid navbar navbar-fixed-top no-padding">
	    <div class="row socials">
	    	<div class="container">
		    	<div class="col-md-5 col-sm-5 pull-right">
					<a href="#"><i class="sitemap fa fa-sitemap"></i></a>
					<a href="#"><i class="youtube  fa fa-youtube-play"></i></a>
					<a href="#"><i class="facebook fa fa-facebook"></i></a>
			    	<a href="index.php"><i class="home fa fa-home"></i></a>
		    	</div>
	    	</div>
	    </div>

		<!--All-->
        <div class="container">
        	<div class="hidden-xs col-sm-12 col-md-12 col-lg-12">
        		<nav class="menu">	
			       <div class=" col-md-2 nav navbar-nav">
	            		<a href="index.php" class="link-logo-header"><img class="logo-header" src="images/logo.png" alt="Logo "></a>
	       			</div>

	       			<div class="col-md-10 col-md-10 menu-header">
			       		<ul class="nav navbar-nav">
			            	<a class="active" href="billetera.php">TU BILLETERA</a>
							<a href="como_funciona.php">COMO SE USA</a>
							<a href="agentes.php">UBÌCANOS</a>
							<a href="faq.php">PREGUNTAS FRECUENTES</a>
							<a href="#">ATENCIÒN AL CLIENTE</a>
							<a href="#">DESCUENTOS</a>
			       		</ul>
	       			</div>
		      	</nav>	
		    </div>
		</div>
		
		<!--Mobile-->
        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
		    <div class="container-fluid navbar navbar-fixed-top">
			    <div class="row socials-mobile">
				    <div class="col-xs-3">
				    	<a href="#"><i class="home fa fa-sitemap"></i></a>
				    </div>
				    <div class="col-xs-3">
				    	<a href="index.php"><i class="home fa fa-home"></i></a>
				    </div>
				    <div class="col-xs-3">
						<a href="#"><i class="youtube  fa fa-youtube-play"></i></a>
				    </div>
				    <div class="col-xs-3">
						<a href="#"><i class="home fa fa-facebook"></i></a>
				    </div>
			    </div>

		    	<div class="container">
		    		<div class="navbar-header">
			    		<a href="index.php" class="navbar-brand">					      
			    			<div class=" col-md-3 nav navbar-nav movile">
								<img class="logo-header" src="images/logo.png" alt="Logo del sitio">
				       		</div>
				       	</a>

			    		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#btn-colapsar">
			    			<span class="sr-only">Navegacion</span>
			    			<span class="icon-bar" style="background: #FFDB00;"></span>
			    			<span class="icon-bar" style="background: #FFDB00;"></span>
			    			<span class="icon-bar" style="background: #FFDB00;"></span>
			    		</button>	
			    	</div>
				
			        <div class="collapse navbar-collapse" id="btn-colapsar">
				       <nav class="menu">	
			       			<div class="col-md-7 pull-left menu-header-mobile">
					       		<ul class="nav navbar-nav">
					            	<li><a class="active" href="billetera.php">TU BILLETERA</a></li>
									<li><a href="como_funciona.php">COMO SE USA</a></li>
									<li><a href="agentes.php">UBÌCANOS</a></li>
									<li><a href="faq.php">PREGUNTAS FRECUENTES</a></li>
									<li><a href="#">ATENCIÒN AL CLIENTE</a></li>
									<li><a href="#">DESCUENTOS</a></li></li>
					       		</ul>
			       			</div>
				       </nav>	
				  </div>
				</div>
			</div>
        </div>       
	</div>
</header>
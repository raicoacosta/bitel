<div class="section-main-block download-app pb-0">
	<div class="container-fluid"> 
		<div class="col-md-4 col-lg-4 hidden-xs hidden-sm">
			<img class="pull-right phone img-responsive" src="images/dashboard.png" alt="Celular">	
		</div>
	 	
	 	<div class="text-contact col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<h4 class="text-blak">Texto mas pequeño del llamado a la acción</h4>
			<p class="text-color"><strong>DESCARGA LA APLICACIÓN Y AFILIATE</strong></p>
		</div>

		<div class="col-sm-6 hidden-xs hidden-md hidden-lg">
			<img class="phone center-block img-responsive" src="images/dashboard.png" alt="Celular">	
		</div>   

		<div class="download-img col-xs-12 col-sm-6 col-md-3 col-lg-3">		
	 		<a href="#"><img class="google-play" src="images/google-play.png" alt="Celular"></a>
	 		<br>	
	 		<a href="#"><img class="appstore" src="images/appstore1.png" alt="Celular"></a>	
	 	</div>	        

		<div class="col-xs-12 col-sm-12 hidden-sm hidden-md hidden-lg">
			<img class="phone center-block img-responsive" src="images/dashboard.png" alt="Celular">	
		</div>
	</div>
</div>
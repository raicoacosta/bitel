
<footer>
	<div class="wrap_content_footer footer">
		<div class="icons container no-padding">
			<div class="col-sm-2 col-xs-6"">
				<div class="footer_icon_wrapper">
					<a href="#">
						<i class="fa fa-user fa-5x"></i>
						<span>Libro de Reclamaciones</span>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6"">
				<div class="footer_icon_wrapper">
					<a href="#">
						<i class="fa fa-user fa-5x"></i>
						<span>Transparencia</span>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6"">
				<div class="footer_icon_wrapper">
					<a href="#">
						<i class="fa fa-user fa-5x"></i>
						<span>¿Quieres ser Agente Bitel?</span>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6"">
				<div class="footer_icon_wrapper">
					<a href="#">
						<i class="fa fa-user fa-5x"></i>
						<span>Contratos</span>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">
				<div class="footer_icon_wrapper">
					<a href="#">
						<i class="fa fa-user fa-5x"></i>
						<span>Denuncia practicas cuestionables</span>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">
		    		<a href="index.php"><i class="footer-home fa fa-home"></i></a>
					<a href="#"><i class="footer-youtube  fa fa-youtube-play"></i></a>
					<a href="#"><i class="footer-facebook fa fa-facebook"></i></a>
	    	</div>
		</div>
	</div>		
		<div class="cr">
			<strong>Todos los derechos reservaodos ©2017</strong>
		</div>	

</footer>
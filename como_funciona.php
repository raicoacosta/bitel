<!DOCTYPE html>
<html>
<?php include "partials/head.php"; ?>
<body>	
	<?php include "partials/header.php"; ?>

	<!--BODY-->
	<section class="img-banner container-fluid no-padding">
		<div class="col-md-12 no-padding">		
			<img class="img-responsive" src="images/banner-billetera.png" alt="La agencia">	
		</div>
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Como Funciona</li>
		</ol>
		<h2 class="title-banner"><strong>COMO FUNCIONA</strong></h2>
	</section>
	<div class="section-main-block container">
		<div class="function">
			<div class="col-md-12">
				<h1><strong>Titulo</strong></h1>

				<p class="subtitle-function text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</p>
			</div>
			<div class="col-md-3 col-md-offset-1 div-item-function">
				<div class="panel-group" id="acordeon">
					<div class="panel panel-default">
						<div class="panel-heading panel-subtitulos" id="heading2">
							<h4 class="panel-title">
								<a class="arrow" href="#colapsable" data-toggle="collapse" data-parent="#acordeon"><strong>COMO SE USA</strong><span class="caret caret-open"></span></a>
							</h4>
						</div>
						<div id="colapsable" class="panel-collapse collapse in" arial-labelledby="heading1">
							<ul class="list-item">
								<a href="#" data-title="Inscribete" data-img="images/mobile-celular.png">
									<li class="active">Inscribete</li>
								</a>
								<a href="#" data-title="Hacer pagos" data-img="images/mobile-celular.png">
									<li>Hacer pagos</li>
								</a>
								<a href="#" data-title="Haz un giro" data-img="images/mobile-celular.png">
									<li>Haz un giro</li>
								</a>
								<a href="#" data-title="Meter plata" data-img="images/mobile-celular.png">
									<li>Meter plata</li>
								</a>
								<a href="#" data-title="Paga tu servicios" data-img="images/mobile-celular.png">
									<li>Paga tu servicios</li>
								</a>
								<a href="#" data-title="Recarga celular" data-img="images/mobile-celular.png">
									<li>Recarga celular</li>
								</a>
								<a href="#" data-title="Sacar plata" data-img="images/mobile-celular.png">
									<li>Sacar plata</li>
								</a>
								<a href="#" data-title="Sacar consulta de saldo" data-img="images/mobile-celular.png">
									<li>Sacar consulta de saldo</li>
								</a>
								<a href="#" data-title="Consulta tus movimientos" data-img="images/mobile-celular.png">
									<li>Consulta tus movimientos</li>
								</a>
								<a href="#" data-title="Cambio de clave secreta" data-img="images/mobile-celular.png">
									<li>Cambio de clave secreta</li>
								</a>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="img-function">
					<img class="center-block img-responsive" src="images/mobile-celular.png">
				</div>
			</div>
			<div class="col-xs-10 col-xs-offset-1  col-sm-8 col-sm-offset-2 col-md-3 col-md-offset-0 content-step">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<h4>Inscribete</h4>
							<div class="step">
								<span class="text-center center-block"><strong>Paso 1</strong></span>
							</div>
							<p class="description-step">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore </p>
						</div>
						<div class="item">
							<h4>Inscribete</h4>
							<div class="step">
								<span class="text-center center-block"><strong>Paso 2</strong></span>
							</div>
							<p class="description-step">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore </p>
						</div>
						<div class="item">
							<h4>Inscribete</h4>
							<div class="step">
								<span class="text-center center-block"><strong>Paso 3</strong></span>
							</div>
							<p class="description-step">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore </p>
						</div>
					</div>
					<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="text-arrow-carousel-left">Aterior</span>
					</a>
					<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
						<span class="text-arrow-carousel-right">Siguiente</span>
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php include "partials/download-app.php"; ?>
	<!--/BODY-->

	<?php include "partials/footer.php"; include "partials/scripts.php"; ?>
	<script type="text/javascript">
		$('.arrow').on('click', function(){
			if ($(this).hasClass('collapsed')){
				$(this).find('span').addClass('caret-open');
			} else{
				$(this).find('span').removeClass('caret-open');
			}
		});
		var eventCarrusel=true;
		$('#myCarousel').on('slide.bs.carousel', function () {
			$('.img-function img').fadeOut(400, function() {
				$('.img-function img').attr('src', $('.list-item a').find('li.active').data('img'));
			}).fadeIn(400);
			eventCarrusel=false;
		})
		$('.list-item a').on('click', function(e){
			e.preventDefault();
			$.each($('.list-item a'), function( index, value ) {
				$(this).find('li').removeClass('active');
			});
			$(this).find('li').addClass('active');
			$('#myCarousel').carousel(0);
			if (eventCarrusel) {
				$('.img-function img').fadeOut(400, function() {
					$('.img-function img').attr('src', $(this).data('img'));
				}).fadeIn(400);
			} 
			$('#myCarousel h4').text($(this).data('title'));
		eventCarrusel = true;
		});
		eventCarrusel = true;
	</script>	
</body>
</html>